import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PedidoRoutingModule } from './pedido-routing.module';
import { PedidoCriarComponent } from './pedido-criar/pedido-criar.component';
import { PedidoEditarComponent } from './pedido-editar/pedido-editar.component';
import { PedidoDetalheComponent } from './pedido-detalhe/pedido-detalhe.component';
import { PedidoComponent } from './pedido.component';
import { CovalentDataTableModule } from '../../../node_modules/@covalent/core/data-table';
import { MatCardModule, MatIconModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    CovalentDataTableModule,
    MatCardModule,
    MatIconModule,
    PedidoRoutingModule
  ],
  declarations: [
    PedidoComponent,
    PedidoCriarComponent,
    PedidoEditarComponent,
    PedidoDetalheComponent
  ],
  exports: []
})
export class PedidoModule { }
