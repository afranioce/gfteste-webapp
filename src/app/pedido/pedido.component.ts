import { Component, OnInit } from '@angular/core';
import { ITdDataTableColumn } from '@covalent/core/data-table';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.scss']
})
export class PedidoComponent implements OnInit {
  configWidthColumns: ITdDataTableColumn[] = [
    { name: 'id', label: 'N.', width: 80 },
    { name: 'valor', label: 'Valor', width: 150 },
    { name: 'fornecedor', label: 'Fornecedor' },
    { name: 'totalItens', label: 'Tot. itens', width: 100 },
  ];

  basicData: any[] = [
    {
      id: 7454,
      valor: 234.23,
      fornecedor: 'Sully sd ewrt 345 34',
      totalItens: 234
    },
    {
      id: 7454,
      valor: 234.23,
      fornecedor: 'Sully',
      totalItens: 234
    }
  ]; // see data json

  constructor() { }

  ngOnInit() { }

}
