import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PedidoEditarComponent } from './pedido-editar/pedido-editar.component';
import { PedidoCriarComponent } from './pedido-criar/pedido-criar.component';
import { PedidoDetalheComponent } from './pedido-detalhe/pedido-detalhe.component';

const routes: Routes = [
  {
    path: 'pedidos/novo',
    component: PedidoCriarComponent,
    data: { title: 'Novo pedido' }
  },
  {
    path: 'pedidos/:id',
    component: PedidoDetalheComponent,
    data: { title: 'Ver pedido' }
  },
  {
    path: 'pedidos/:id/editar',
    component: PedidoEditarComponent,
    data: { title: 'Editar pedido' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PedidoRoutingModule { }
