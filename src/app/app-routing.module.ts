import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FornecedorComponent } from './fornecedor/fornecedor.component';
import { PedidoComponent } from './pedido/pedido.component';
import { ProdutoComponent } from './produto/produto.component';

const routes: Routes = [
  {
    path: 'fornecedores',
    component: FornecedorComponent,
    data: { title: 'Fornecedores' }
  },
  {
    path: 'pedidos',
    component: PedidoComponent,
    data: { title: 'Pedidos' }
  },
  {
    path: 'produtos',
    component: ProdutoComponent,
    data: { title: 'Produtos' }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
