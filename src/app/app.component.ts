import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  titulo = 'GF Teste';
  rotas: Object[] = [
    {
      icone: 'home',
      rota: '.',
      titulo: 'Home',
    },
    {
      icone: 'library_books',
      rota: 'fornecedores',
      titulo: 'Fornecedores',
    },
    {
      icone: 'color_lens',
      rota: 'produtos',
      titulo: 'Produtos',
    },
    {
      icone: 'view_quilt',
      rota: 'pedidos',
      titulo: 'Pedidos',
    }
  ];
}
