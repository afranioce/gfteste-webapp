import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FornecedorRoutingModule } from './fornecedor-routing.module';
import { FornecedorComponent } from './fornecedor.component';
import { MatCardModule, MatIconModule } from '@angular/material';
@NgModule({
  imports: [
    MatCardModule,
    MatIconModule,
    CommonModule,
    FornecedorRoutingModule
  ],
  declarations: [
    FornecedorComponent
  ]
})
export class FornecedorModule { }
